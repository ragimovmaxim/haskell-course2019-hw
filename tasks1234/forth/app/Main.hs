module Main where

import AST

main :: IO ()
main = do
  print fib 
  where
    fib = 
      [Def "fib"
        [Push 0
        , Push 1
        , Word "rot"
        , Push 0
        , Loop [
          Word "over"
          , BinOp (getBinOp (+))
          , Word "swap"
          ] 
        , Word "drop"
        ]
      ]
